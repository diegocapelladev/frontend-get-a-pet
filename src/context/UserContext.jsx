import { createContext } from 'react'
import Types from 'prop-types'

import useAuth from '../hooks/useAuth'

const Context = createContext()

const UserProvider = ({ children }) => {
  const { register, authenticated, loading, login, logout } = useAuth()

  return (
    <Context.Provider
      value={{ register, authenticated, loading, login, logout }}
    >
      {children}
    </Context.Provider>
  )
}

export { Context, UserProvider }

UserProvider.propTypes = {
  children: Types.node
}
