import * as S from './styles'

const Footer = () => (
  <S.Wrapper>
    <p>Copyright &copy; 2022 - Diego Capella</p>
  </S.Wrapper>
)

export default Footer
