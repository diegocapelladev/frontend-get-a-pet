import styled, { css } from 'styled-components'

export const Wrapper = styled.footer`
  ${({ theme }) => css`
    height: 6rem;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: ${theme.colors.lightGray};
    color: ${theme.colors.gray};
  `}
`
