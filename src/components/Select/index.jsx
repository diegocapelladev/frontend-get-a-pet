import Types from 'prop-types'

import * as S from './styles'

const Select = ({ label, name, options, handleOnChange, value }) => {
  return (
    <S.Wrapper>
      <S.Label htmlFor={name}>{label}: </S.Label>
      <S.Select name={name} id={name} onChange={handleOnChange} value={value}>
        <option>Selecione uma opção</option>
        {options.map((option, index) => (
          <option key={index} value={option}>
            {option}
          </option>
        ))}
      </S.Select>
    </S.Wrapper>
  )
}

export default Select

Select.propTypes = {
  label: Types.string,
  name: Types.string,
  options: Types.array,
  handleOnChange: Types.func,
  value: Types.any
}
