import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  margin: 2rem auto;
`

export const Label = styled.label`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.xlarge};
  `}
`
export const Select = styled.select`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.large};
  `}

`
