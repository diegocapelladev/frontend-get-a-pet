import styled, { css } from 'styled-components'

export const AccountLink = styled.p`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 3rem;

    a {
      margin-left: 1rem;
      font-weight: ${theme.font.bold};
      color: ${theme.colors.primary};;
      text-decoration: none;
      cursor: pointer;

      &:hover {
        text-decoration: underline;
      }
    }
  `}
`
