import Types from 'prop-types'
import { Link } from 'react-router-dom'

import Image from '../Image'
import * as S from './styles'

const PetItem = ({ pet }) => {
  return (
    <S.Wrapper>
      <Image
        src={`${import.meta.env.VITE_APP_API}/images/pets/${pet.images[0]}`}
        alt={pet.name}
      />
      <S.Name>{pet.name}</S.Name>

      <S.Weight>
        <span>Peso:</span> {pet.weight} kg
      </S.Weight>

      {pet.available ? (
        <Link to={`/pet/${pet._id}`}>Mais detalhes</Link>
      ) : (
        <S.Adopted>Adotado!</S.Adopted>
      )}
    </S.Wrapper>
  )
}
export default PetItem

PetItem.propTypes = {
  pet: Types.shape({
    _id: Types.string,
    images: Types.array,
    name: Types.string,
    weight: Types.number,
    available: Types.bool
  })
}
