import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background-color: ${theme.colors.lightBg};
    padding: 5rem;
    border-radius: ${theme.border.radius};
    max-width: 40rem;

    a {
      text-decoration: none;
      color: ${theme.colors.white};
      background: ${theme.colors.green};
      padding: 1rem 2rem;
      margin-top: 2rem;
      border-radius: ${theme.border.radius};
      cursor: pointer;
      transition: 0.2s ease-in-out;

      &:hover {
        background: ${theme.colors.greenHover};
      }
    }
  `}
`

export const Name = styled.h3``

export const Weight = styled.p`

  span {
    font-weight: bold;
  }
`
export const Adopted = styled.p`
  ${({ theme }) => css`
    color: ${theme.colors.white};
    background: ${theme.colors.red};
    padding: 1rem 2rem;
    margin-top: 2rem;
    border-radius: ${theme.border.radius};
  `}
`
