import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`

export const Label = styled.label`
  font-size: 2rem;
  margin-bottom: 0.5rem;
`

export const Input = styled.input`
  ${({ theme }) => css`
    font-size: 1.8rem;
    padding: 1rem;
    border: .2rem solid ${theme.colors.lightGray};
    outline: none;
    border-radius: 0.5rem;
    margin-bottom: 1.5rem;

    &:focus {
      border-color: ${theme.colors.gray};
    }

    &::placeholder {
      color: ${theme.colors.gray};
    }
  `}
`
