import Types from 'prop-types'

import * as S from './styles'

const FormField = ({
  label,
  type,
  name,
  placeholder,
  handleOnChange,
  value,
  multiple
}) => {
  return (
    <S.Wrapper>
      <S.Label htmlFor={name}>{label}: </S.Label>
      <S.Input
        type={type}
        name={name}
        id={name}
        placeholder={placeholder}
        value={value}
        onChange={handleOnChange}
        {...(multiple ? { multiple } : '')}
        autoComplete="off"
      />
    </S.Wrapper>
  )
}
export default FormField

FormField.propTypes = {
  label: Types.string.isRequired,
  type: Types.string.isRequired,
  name: Types.string.isRequired,
  placeholder: Types.string,
  handleOnChange: Types.func,
  value: Types.string,
  multiple: Types.any
}
