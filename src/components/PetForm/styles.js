import styled from 'styled-components'

export const Wrapper = styled.main``

export const Preview = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`
