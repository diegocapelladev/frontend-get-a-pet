import { useState } from 'react'
import Types from 'prop-types'

import FormField from '../FormField'
import Select from '../Select'
import { Button } from '../AccountButton'
import * as S from './styles'
import Image from '../Image'

const PetForm = ({ petData, handleSubmit, btnText }) => {
  const [pet, setPet] = useState(petData || {})
  const [preview, setPreview] = useState([])
  const colors = ['Branco', 'Preto', 'Cinza', 'Caramelo']

  const handleFileChange = (e) => {
    setPreview(Array.from(e.target.files))
    setPet({ ...pet, images: [...e.target.files] })
  }

  const handleChange = (e) => {
    setPet({ ...pet, [e.target.name]: e.target.value })
  }

  const handleColor = (e) => {
    setPet({ ...pet, color: e.target.options[e.target.selectedIndex].text })
  }

  const submit = (e) => {
    e.preventDefault()
    handleSubmit(pet)
  }

  return (
    <S.Wrapper>
      <form onSubmit={submit}>
        <S.Preview>
          {preview.length > 0
            ? preview.map((image, index) => (
                <Image
                  src={URL.createObjectURL(image)}
                  alt={pet.name}
                  key={`${pet.name}+${index}`}
                />
              ))
            : pet.images &&
              pet.images.map((image, index) => (
                <Image
                  src={`${import.meta.env.VITE_APP_API}/images/pets/${image}`}
                  alt={pet.name}
                  key={`${pet.name}+${index}`}
                />
              ))}
        </S.Preview>
        <FormField
          label="Imagem"
          type="file"
          name="image"
          handleOnChange={handleFileChange}
          multiple={true}
        />
        <FormField
          label="Nome do Pet"
          type="text"
          name="name"
          handleOnChange={handleChange}
          value={pet.name || ''}
        />
        <FormField
          label="Idade do Pet"
          type="number"
          name="age"
          handleOnChange={handleChange}
          value={pet.age || ''}
        />
        <FormField
          label="Peso do Pet"
          type="number"
          name="weight"
          handleOnChange={handleChange}
          value={pet.weight || ''}
        />
        <Select
          name="color"
          label="Selecione a categoria"
          options={colors}
          handleOnChange={handleColor}
          value={pet.color || ''}
        />
        <Button type="submit" value={btnText}>
          {btnText}
        </Button>
      </form>
    </S.Wrapper>
  )
}
export default PetForm

PetForm.propTypes = {
  petData: Types.object,
  handleSubmit: Types.func,
  btnText: Types.string
}
