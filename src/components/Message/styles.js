import styled, { css } from 'styled-components'

const modifiers = {
  success: (theme) => css`
    color: ${theme.colors.white};
    background-color: ${theme.colors.green};
  `,

  error: (theme) => css`
    color: ${theme.colors.white};
    background-color: ${theme.colors.red};
  `
}

export const Wrapper = styled.div`
  ${({ theme, type }) => css`
    width: 100%;
    padding: 1rem;
    margin: 1.5rem auto 0;
    border-radius: ${theme.border.radius};
    text-align: center;

    ${type && modifiers[type](theme)}
  `}
`
