// import { useState } from 'react'
import { useEffect, useState } from 'react'

import { Container } from '../Container'
import bus from '../../utils/bus'

import * as S from './styles'

const Message = () => {
  const [visibility, setVisibility] = useState(false)
  const [message, setMessage] = useState('')
  const [type, setType] = useState('')

  useEffect(() => {
    bus.addListener('flash', ({ message, type }) => {
      setVisibility(true)
      setMessage(message)
      setType(type)

      setTimeout(() => {
        setVisibility(false)
      }, 3000)
    })
  }, [])

  return (
    visibility && (
      <Container>
        <S.Wrapper type={type}>{message}</S.Wrapper>
      </Container>
    )
  )
}

export default Message
