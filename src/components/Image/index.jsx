import Types from 'prop-types'
import styled from 'styled-components'

export const Img = styled.img`
  margin: 0 auto;
  display: flex;
  justify-content: center;
  width: 20rem;
  height: 20rem;
  border-radius: 50%;
  object-fit: cover;
  margin-bottom: 3rem;
`

const Image = ({ src, alt }) => <Img src={src} alt={alt} />

export default Image

Image.propTypes = {
  src: Types.string,
  alt: Types.string
}
