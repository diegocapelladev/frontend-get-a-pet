import styled, { css } from 'styled-components'

export const Title = styled.h2`
  ${({ theme }) => css`
    color: ${theme.colors.primary};
    font-size: ${theme.font.sizes.huge};
    text-align: center;
    margin-bottom: 4rem;
    border-bottom: 0.1rem solid ${theme.colors.lightGray};
  `}
`
