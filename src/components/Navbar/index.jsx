import { useContext } from 'react'
import { Link } from 'react-router-dom'

import { Context } from '../../context/UserContext'
import * as S from './styles'

const Navbar = () => {
  const { authenticated, logout } = useContext(Context)

  return (
    <S.Navbar>
      <S.Logo>
        <h2>Get a Pet</h2>
      </S.Logo>

      <S.List>
        <S.ListItem>
          <Link to="/">Adotar</Link>
        </S.ListItem>
        {authenticated ? (
          <>
            <S.ListItem>
              <Link to="/pet/myadoptions">Minhas Adoções</Link>
            </S.ListItem>

            <S.ListItem>
              <Link to="/pet/mypets">Meus Pets</Link>
            </S.ListItem>

            <S.ListItem>
              <Link to="/profile">Perfil</Link>
            </S.ListItem>

            <S.ListItem>
              <a onClick={logout}>Sair</a>
            </S.ListItem>
          </>
        ) : (
          <>
            <S.ListItem>
              <Link to="/login">Entrar</Link>
            </S.ListItem>

            <S.ListItem>
              <Link to="/register">Cadastrar</Link>
            </S.ListItem>
          </>
        )}
      </S.List>
    </S.Navbar>
  )
}

export default Navbar
