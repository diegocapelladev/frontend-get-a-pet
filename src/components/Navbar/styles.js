import styled, { css } from 'styled-components'

export const Navbar = styled.nav`
  ${({ theme }) => css`
    display: flex;
    justify-content: space-between;
    padding: 2rem;
    background-color: ${theme.colors.secondary};
  `}
`
export const Logo = styled.div`
  display: flex;
  align-items: center;
`

export const List = styled.ul`
  display: flex;
  align-items: center;
  list-style: none;
`

export const ListItem = styled.li`
  ${({ theme }) => css`

    a {
      color: ${theme.colors.primary};
      text-decoration: none;
      font-weight: bold;
      cursor: pointer;
      padding: .8rem 2rem;

      &:hover {
        background-color: ${theme.colors.primary};
        color: ${theme.colors.white};
        transition: .5s ease-out;
        border-radius: .5rem;
      }
    }
  `}
`
