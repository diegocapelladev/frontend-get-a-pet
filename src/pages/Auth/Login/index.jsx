import { useContext, useState } from 'react'
import { Link } from 'react-router-dom'

import { Button } from '../../../components/AccountButton'
import { AccountLink } from '../../../components/AccountLink'
import { Container } from '../../../components/Container'
import { Title } from '../../../components/TitleForm'
import { Context } from '../../../context/UserContext'
import * as S from './styles'
import FormField from '../../../components/FormField'

const Login = () => {
  const [user, setUser] = useState({})
  const { login } = useContext(Context)

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    login(user)
  }

  return (
    <S.Wrapper>
      <Container>
        <Title>Login</Title>

        <S.Form onSubmit={handleSubmit}>
          <FormField
            label="E-mail"
            type="email"
            name="email"
            placeholder="E-mail"
            handleOnChange={handleChange}
          />

          <FormField
            label="Senha"
            type="password"
            name="password"
            placeholder="Senha"
            handleOnChange={handleChange}
          />
          <Button type="submit" value="Entrar">
            Entrar
          </Button>
        </S.Form>

        <AccountLink>
          Para criar uma conta: <Link to="/register">Clique aqui</Link>
        </AccountLink>
      </Container>
    </S.Wrapper>
  )
}

export default Login
