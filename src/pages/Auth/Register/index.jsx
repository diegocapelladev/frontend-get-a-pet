import { useContext, useState } from 'react'
import { Link } from 'react-router-dom'

import { Button } from '../../../components/AccountButton'
import { AccountLink } from '../../../components/AccountLink'
import { Container } from '../../../components/Container'
import { Title } from '../../../components/TitleForm'
import { Context } from '../../../context/UserContext'
import FormField from '../../../components/FormField'

import * as S from './styles'

const Register = () => {
  const [user, setUser] = useState({})
  const { register } = useContext(Context)

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value })
  }

  const handleSubmit = (e) => {
    e.preventDefault()

    register(user)
  }

  return (
    <S.Wrapper>
      <Container>
        <Title>Registrar</Title>

        <S.Form onSubmit={handleSubmit}>
          <FormField
            label="Nome"
            type="text"
            name="name"
            placeholder="Nome Completo"
            handleOnChange={handleChange}
          />
          <FormField
            label="E-mail"
            type="email"
            name="email"
            placeholder="E-mail"
            handleOnChange={handleChange}
          />
          <FormField
            label="Telefone"
            type="tel"
            name="phone"
            placeholder="Telefone"
            handleOnChange={handleChange}
          />
          <FormField
            label="Senha"
            type="password"
            name="password"
            placeholder="Senha"
            handleOnChange={handleChange}
          />
          <FormField
            label="Confirme a Senha"
            type="password"
            name="confirmpassword"
            placeholder="Confirme a Senha"
            handleOnChange={handleChange}
          />
          <Button type="submit" value="Cadastrar">
            Cadastrar
          </Button>
        </S.Form>

        <AccountLink>
          Ja tem uma conta? <Link to="/login">Clique aqui</Link>
        </AccountLink>
      </Container>
    </S.Wrapper>
  )
}

export default Register
