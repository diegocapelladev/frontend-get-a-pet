import styled, { css } from 'styled-components'

export const Wrapper = styled.main`
  margin: 5rem auto;
`
export const Title = styled.h1`
  ${({ theme }) => css`
    color: ${theme.colors.primary};
    text-align: center;
    margin-bottom: 3rem;
  `}
`
export const Content = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 2rem;
`

export const Item = styled.div`
  ${({ theme }) => css`
    background: ${theme.colors.lightBg};
    padding: 2rem;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  `}
`

export const PetName = styled.h2`
  ${({ theme }) => css`
    color: ${theme.colors.primary};
    font-size: ${theme.font.sizes.xlarge};
  `}
`

export const Contacts = styled.div`
  ${({ theme }) => css`
    span {
      color: ${theme.colors.primary};
      font-weight: 500;
    }
  `}
`
export const Actions = styled.div`
  ${({ theme }) => css`
    margin-top: 1rem;
    padding: 1rem 3rem;
    background-color: ${theme.colors.secondary};
    color: ${theme.colors.gray};
    font-weight: 500;
    border-radius: ${theme.border.radius};
  `}
`
