import { useEffect, useState } from 'react'

import api from '../../utils/api'

import { Container } from '../../components/Container'
import Image from '../../components/Image'
import * as S from './styles'

const Adoptions = () => {
  const [pets, setPets] = useState({})
  const [token] = useState(localStorage.getItem('token') || '')

  useEffect(() => {
    api
      .get('/pets/myadoptions', {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`
        }
      })
      .then((response) => {
        setPets(response.data.pets)
      })
  }, [token])

  return (
    <Container>
      <S.Wrapper>
        <S.Title>Adoptions</S.Title>

        <S.Content>
          {pets.length > 0 &&
            pets.map((pet) => (
              <S.Item key={pet._id}>
                <Image
                  src={`${import.meta.env.VITE_APP_API}/images/pets/${
                    pet.images[0]
                  }`}
                  alt={pet.name}
                />

                <S.PetName>{pet.name}</S.PetName>
                <S.Contacts>
                  <p>
                    <span>Ligue para: </span> {pet.user.phone}
                  </p>
                  <p>
                    <span>Fale com: </span> {pet.user.name}
                  </p>
                </S.Contacts>
                <S.Actions>
                  {pet.available ? (
                    <p>Adoção em processo</p>
                  ) : (
                    <p>Parabéns por concluir a adoção</p>
                  )}
                </S.Actions>
              </S.Item>
            ))}
        </S.Content>
        {pets.length === 0 && <p>Ainda não há pets adotados!</p>}
      </S.Wrapper>
    </Container>
  )
}

export default Adoptions
