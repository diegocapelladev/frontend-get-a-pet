import styled, { css } from 'styled-components'

export const Wrapper = styled.main`
  margin: 8rem auto;
`

export const Header = styled.div`
  text-align: center;
  margin-bottom: 5rem;

  h1 {
    ${({ theme }) => css`
      color: ${theme.colors.primary};
    `}
  }
`

export const Content = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 2rem;
`
