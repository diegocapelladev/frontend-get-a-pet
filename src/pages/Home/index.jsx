import { useEffect, useState } from 'react'

import api from '../../utils/api'

import { Container } from '../../components/Container'
import PetItem from '../../components/PetItem'
import * as S from './styles'

const Home = () => {
  const [pets, setPets] = useState({})

  useEffect(() => {
    api.get('/pets').then((response) => {
      setPets(response.data.pets)
    })
  }, [])

  return (
    <S.Wrapper>
      <Container>
        <S.Header>
          <h1>Adote um Pet</h1>
          <p>Veja os detalhes de cada um e conheça o tutor deles</p>
        </S.Header>

        <S.Content>
          {pets.length > 0 &&
            pets.map((pet) => <PetItem key={pet._id} pet={pet} />)}
        </S.Content>
        {pets.length === 0 && <p>Não há Pets cadastrados</p>}
      </Container>
    </S.Wrapper>
  )
}
export default Home
