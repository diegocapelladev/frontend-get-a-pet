import { useEffect, useState } from 'react'

import api from '../../utils/api'
import { Title } from '../../components/TitleForm'
import { Container } from '../../components/Container'
import { Button } from '../../components/AccountButton'
import FormField from '../../components/FormField'
import useFlashMessage from '../../hooks/useFlashMessage'

import * as S from './styles'
import Image from '../../components/Image'

const Profile = () => {
  const [user, setUser] = useState({})
  const [preview, setPreview] = useState()
  const [token] = useState(localStorage.getItem('token') || '')
  const { setFlashMessage } = useFlashMessage()

  useEffect(() => {
    api
      .get('/users/checkuser', {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`
        }
      })
      .then((response) => {
        setUser(response.data)
      })
  }, [token])

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value })
  }

  const handleFileChange = (e) => {
    setPreview(e.target.files[0])
    setUser({ ...user, [e.target.name]: e.target.files[0] })
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    let msgType = 'success'
    const formData = new FormData()

    const userFormData = Object.keys(user).forEach((key) => {
      formData.append(key, user[key])
    })

    formData.append('user', userFormData)

    const data = await api
      .patch(`/users/edit/${user.id}`, formData, {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`,
          'Content-Type': 'multipart/form-data'
        }
      })
      .then((response) => {
        return response.data
      })
      .catch((err) => {
        msgType = 'error'
        return err.response.data
      })

    setFlashMessage(data.message, msgType)
  }

  return (
    <S.Wrapper>
      <Container>
        <Title>Perfil</Title>

        {(user.image || preview) && (
          <Image
            src={
              preview
                ? URL.createObjectURL(preview)
                : `${import.meta.env.VITE_APP_API}/images/users/${user.image}`
            }
            alt={user.name}
          />
        )}

        <S.Form onSubmit={handleSubmit}>
          <FormField
            label="Imagem"
            type="file"
            name="image"
            handleOnChange={handleFileChange}
          />

          <FormField
            label="E-mail"
            type="text"
            name="email"
            value={user.email || ''}
            handleOnChange={handleChange}
            placeholder="E-mail"
          />

          <FormField
            label="Nome"
            type="text"
            name="name"
            value={user.name || ''}
            handleOnChange={handleChange}
            placeholder="Nome Completo"
          />

          <FormField
            label="Telefone"
            type="tel"
            name="phone"
            value={user.phone || ''}
            handleOnChange={handleChange}
            placeholder="Telefone"
          />

          <FormField
            label="Senha"
            type="password"
            name="password"
            value={user.password || ''}
            handleOnChange={handleChange}
            placeholder="Senha"
          />

          <FormField
            label="Confirme a Senha"
            type="password"
            name="confirmpassword"
            value={user.confirmpassword || ''}
            handleOnChange={handleChange}
            placeholder="Senha"
          />

          <Button type="submit" value="Editar">
            Salvar
          </Button>
        </S.Form>
      </Container>
    </S.Wrapper>
  )
}

export default Profile
