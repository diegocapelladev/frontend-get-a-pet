import styled, { css } from 'styled-components'

export const Wrapper = styled.main`
  margin: 5rem auto;
`
export const Header = styled.div`
  ${({ theme }) => css`
    text-align: center;
    margin-bottom: 3rem;

    h1 {
      color: ${theme.colors.primary};
    }
  `}
`

export const ImageSection = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 2rem;
`

export const Image = styled.img`
  max-width: 30rem;
`
export const Details = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 2rem 0;
    font-size: ${theme.font.sizes.large};

    span {
      font-weight: 500;
      color: ${theme.colors.primary};
    }
  `}
`
export const Button = styled.button`
   ${({ theme }) => css`
   font-size: ${theme.font.sizes.medium};
    color: ${theme.colors.white};
    background: ${theme.colors.green};
    border: none;
    padding: 1rem 2rem;
    margin-top: 2rem;
    border-radius: ${theme.border.radius};
    cursor: pointer;

    &:hover {
      filter: brightness(0.9);
    }
  `}
`
