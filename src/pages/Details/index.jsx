import { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'

import api from '../../utils/api'
import useFlashMessage from '../../hooks/useFlashMessage'

import { Container } from '../../components/Container'
import * as S from './styles'

const Details = () => {
  const [pet, setPet] = useState({})
  const { id } = useParams()
  const { setFlashMessage } = useFlashMessage()
  const [token] = useState(localStorage.getItem('token') || '')

  useEffect(() => {
    api.get(`/pets/${id}`).then((response) => {
      setPet(response.data.pet)
    })
  }, [id])

  const schedule = async () => {
    let msgType = 'success'

    const data = await api
      .patch(`/pets/schedule/${pet._id}`, {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`
        }
      })
      .then((response) => {
        return response.data
      })
      .catch((err) => {
        msgType = 'error'
        return err.response.data
      })

    setFlashMessage(data.message, msgType)
  }

  return (
    <Container>
      {pet.name && (
        <S.Wrapper>
          <S.Header>
            <h1>Conhecendo o Pet: {pet.name}</h1>
            <p>Se tiver interesse, marque uma visita para conhecê-lo!</p>
          </S.Header>

          <S.ImageSection>
            {pet.images.map((image, index) => (
              <S.Image
                key={index}
                src={`${import.meta.env.VITE_APP_API}/images/pets/${image}`}
                alt={pet.name}
              />
            ))}
          </S.ImageSection>

          <S.Details>
            <p>
              <span>Peso: </span> {pet.weight} kg
            </p>
            <p>
              <span>Idade: </span> {pet.age} anos
            </p>

            {token ? (
              <S.Button onClick={schedule}>Solicitar visita</S.Button>
            ) : (
              <p>
                Você precisa <Link to="/register">criar uma conta</Link> para
                solicitar a visita.
              </p>
            )}
          </S.Details>
        </S.Wrapper>
      )}
    </Container>
  )
}

export default Details
