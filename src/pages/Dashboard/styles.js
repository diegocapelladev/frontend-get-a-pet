import styled, { css } from 'styled-components'

export const Wrapper = styled.main`
  margin: 4rem auto;
`

export const Title = styled.h2`
  ${({ theme }) => css`
    color: ${theme.colors.primary};
  `}
`
export const Header = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 2rem;

  a {
    text-decoration: none;
    padding: 1rem 2rem;
    background-color: ${theme.colors.primary};
    color: ${theme.colors.white};
    border-radius: ${theme.border.radius};
    font-weight: 500;
    box-shadow: 0px 10px 15px -4px rgba(0,0,0,0.2);
    transition: 0.2s ease-in-out;

    &:hover {
      background-color: ${theme.colors.secondary};
    color: ${theme.colors.gray};
    }
  }
  `}
`

export const Content = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 2rem;
`

export const Item = styled.div`
  ${({ theme }) => css`
    background: ${theme.colors.lightBg};
    padding: 2rem;
  `}
`
export const Buttons = styled.div`
  margin-top: 2rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const ConcluiAdoption = styled.button`
  ${({ theme }) => css`
    background: ${theme.colors.green};
    color: ${theme.colors.white};
    font-size: ${theme.font.sizes.medium};
    border-radius: ${theme.border.radius};
    padding: 1rem 2rem;
    border: none;
    cursor: pointer;
    transition: 0.2s ease-in-out;

    &:hover {
      filter: brightness(0.9);
    }
  `}
`

export const Edit = styled.span`
  ${({ theme }) => css`
    background: ${theme.colors.secondary};
    border-radius: ${theme.border.radius};
    padding: 1rem 2rem;
    border: none;
    cursor: pointer;
    transition: 0.2s ease-in-out;

    &:hover {
      filter: brightness(0.9);
    }

    a {
      text-decoration: none;
      color: ${theme.colors.primary};
      font-size: ${theme.font.sizes.medium};
    }
  `}
`

export const Delete = styled.button`
  ${({ theme }) => css`
    background: ${theme.colors.red};
    color: ${theme.colors.white};
    font-size: ${theme.font.sizes.medium};
    border-radius: ${theme.border.radius};
    padding: 1rem 2rem;
    border: none;
    cursor: pointer;
    transition: 0.2s ease-in-out;

    &:hover {
      filter: brightness(0.9);
    }
  `}
`
