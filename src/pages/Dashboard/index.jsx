import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import api from '../../utils/api'
import useFlashMessage from '../../hooks/useFlashMessage'

import { Container } from '../../components/Container'
import Image from '../../components/Image'
import * as S from './styles'

const Dashboard = () => {
  const [pets, setPets] = useState([])
  const [token] = useState(localStorage.getItem('token') || '')
  const { setFlashMessage } = useFlashMessage()

  useEffect(() => {
    api
      .get('pets/mypets', {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`
        }
      })
      .then((response) => {
        setPets(response.data.pets)
      })
  }, [token])

  const concludeAdoption = async (id) => {
    let msgType = 'success'

    const data = await api
      .patch(`/pets/concluded/${id}`, {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`
        }
      })
      .then((response) => {
        return response.data
      })
      .catch((err) => {
        msgType = 'error'
        return err.response.data
      })

    setFlashMessage(data.message, msgType)
  }

  const removePet = async (id) => {
    let msgType = 'success'

    const data = await api
      .delete(`/pets/${id}`, {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`
        }
      })
      .then((response) => {
        const updatedPets = pets.filter((pet) => pet._id != id)
        setPets(updatedPets)
        return response.data
      })
      .catch((err) => {
        msgType = 'error'
        return err.response.data
      })

    setFlashMessage(data.message, msgType)
  }

  return (
    <S.Wrapper>
      <Container>
        <S.Header>
          <S.Title>Meus Pets</S.Title>
          <Link to="/pet/add">Cadastrar Pet</Link>
        </S.Header>

        <S.Content>
          {pets.length > 0 &&
            pets.map((pet) => (
              <S.Item key={pet._id}>
                <Image
                  src={`${import.meta.env.VITE_APP_API}/images/pets/${
                    pet.images[0]
                  }`}
                  alt={pet.name}
                />
                <span>{pet.name}</span>
                <div>
                  {pet.available ? (
                    <S.Buttons>
                      {pet.adopter && (
                        <S.ConcluiAdoption
                          onClick={() => {
                            concludeAdoption(pet._id)
                          }}
                        >
                          Concluir adoção
                        </S.ConcluiAdoption>
                      )}

                      <S.Edit>
                        <Link to={`/pet/edit/${pet._id}`}>Editar</Link>
                      </S.Edit>

                      <S.Delete onClick={() => removePet(pet._id)}>
                        Excluir
                      </S.Delete>
                    </S.Buttons>
                  ) : (
                    <p>Pet já Adotado</p>
                  )}
                </div>
              </S.Item>
            ))}
        </S.Content>
        {pets.length === 0 && <p>Não há Pets cadastrados</p>}
      </Container>
    </S.Wrapper>
  )
}

export default Dashboard
