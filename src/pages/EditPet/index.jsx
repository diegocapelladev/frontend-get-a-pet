import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

import api from '../../utils/api'
import useFlashMessage from '../../hooks/useFlashMessage'

import PetForm from '../../components/PetForm'
import { Container } from '../../components/Container'
import * as S from './styles'

const EditPet = () => {
  const [pet, setPet] = useState({})
  const [token] = useState(localStorage.getItem('token') || '')
  const { id } = useParams()
  const { setFlashMessage } = useFlashMessage()

  useEffect(() => {
    api
      .get(`/pets/${id}`, {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`
        }
      })
      .then((response) => {
        setPet(response.data.pet)
      })
  }, [token, id])

  const updatePet = async (pet) => {
    let msgType = 'success'

    const formData = new FormData()

    const petFormData = await Object.keys(pet).forEach((key) => {
      if (key === 'images') {
        for (let i = 0; i < pet[key].length; i++) {
          formData.append(`images`, pet[key][i])
        }
      } else {
        formData.append(key, pet[key])
      }
    })

    formData.append('pet', petFormData)

    const data = await api
      .patch(`/pets/${pet._id}`, formData, {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`,
          'Content-type': 'multipart/form-data'
        }
      })
      .then((response) => {
        return response.data
      })
      .catch((err) => {
        msgType = 'error'
        return err.response.data
      })

    setFlashMessage(data.message, msgType)
  }

  return (
    <S.Wrapper>
      <Container>
        <S.Heading>
          <h1>Editando o Pet: {pet.name}</h1>
          <p>Depois da edição os dados serão atualizados no sistema</p>
        </S.Heading>
        {pet.name && (
          <PetForm handleSubmit={updatePet} petData={pet} btnText="Editar" />
        )}
      </Container>
    </S.Wrapper>
  )
}

export default EditPet
