import styled, { css } from 'styled-components'

export const Wrapper = styled.main`
  margin: 5rem auto;
`
export const Heading = styled.div`
  ${({ theme }) => css`
    text-align: center;
    margin-bottom: 2rem;

    h1 {
      color: ${theme.colors.primary};
    }
  `}
`
