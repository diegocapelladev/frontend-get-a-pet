import { useState } from 'react'
import { useNavigate } from 'react-router-dom'

import useFlashMessage from '../../hooks/useFlashMessage'
import api from '../../utils/api'

import PetForm from '../../components/PetForm'
import { Container } from '../../components/Container'
import { Title } from '../../components/TitleForm'
import * as S from './styles'

const AddPet = () => {
  const [token] = useState(localStorage.getItem('token') || '')
  const { setFlashMessage } = useFlashMessage()
  const navigate = useNavigate()

  const registerPet = async (pet) => {
    let msgType = 'success'

    const formData = new FormData()

    const petFormData = await Object.keys(pet).forEach((key) => {
      if (key === 'images') {
        for (let i = 0; i < pet[key].length; i++) {
          formData.append(`images`, pet[key][i])
        }
      } else {
        formData.append(key, pet[key])
      }
    })

    formData.append('pet', petFormData)

    const data = await api
      .post(`/pets/create`, formData, {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`,
          'Content-type': 'multipart/form-data'
        }
      })
      .then((response) => {
        return response.data
      })
      .catch((err) => {
        msgType = 'error'
        return err.response.data
      })

    setFlashMessage(data.message, msgType)
    navigate('/pet/mypets')
  }

  return (
    <Container>
      <S.Wrapper>
        <Title>Cadastre um Pet</Title>

        <PetForm handleSubmit={registerPet} btnText="Cadastrar" />
      </S.Wrapper>
    </Container>
  )
}

export default AddPet
