import { BrowserRouter, Route, Routes } from 'react-router-dom'

import Home from './pages/Home'
import Login from './pages/Auth/Login'
import Register from './pages/Auth/Register'
import Profile from './pages/User'

import Navbar from './components/Navbar'
import Footer from './components/Footer'
import Message from './components/Message'

import { UserProvider } from './context/UserContext'
import Dashboard from './pages/Dashboard'
import AddPet from './pages/AddPet'
import Details from './pages/Details'
import EditPet from './pages/EditPet'
import Adoptions from './pages/Adoptions'

const App = () => {
  return (
    <BrowserRouter>
      <UserProvider>
        <Navbar />
        <Message />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/pet/mypets" element={<Dashboard />} />
          <Route path="/pet/add" element={<AddPet />} />
          <Route path="/pet/:id" element={<Details />} />
          <Route path="/pet/edit/:id" element={<EditPet />} />
          <Route path="/pet/myadoptions" element={<Adoptions />} />
        </Routes>
        <Footer />
      </UserProvider>
    </BrowserRouter>
  )
}

export default App
