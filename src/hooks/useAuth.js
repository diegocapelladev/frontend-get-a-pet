import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

import api from '../utils/api'
import useFlashMessage from './useFlashMessage'

const useAuth = () => {
  const [authenticated, setAuthenticated] = useState(false)
  const [loading, setLoading] = useState(true)
  const { setFlashMessage } = useFlashMessage()
  const navigate = useNavigate()

  useEffect(() => {
    const token = localStorage.getItem('token')

    if (token) {
      api.defaults.headers.Authorization = `Bearer ${JSON.parse(token)}`
      setAuthenticated(true)
    }

    setLoading(false)
  }, [])

  async function register(user) {
    let msgText = 'Cadastro realizado com sucesso!'
    let msgType = 'success'

    try {
      const data = await api.post('/users/register', user).then((response) => {
        return response.data
      })

      await authUser(data)
    } catch (err) {
      msgText = err.response.data.message
      msgType = 'error'
    }

    setFlashMessage(msgText, msgType)
  }

  async function authUser(data) {
    setAuthenticated(true)

    localStorage.setItem('token', JSON.stringify(data.token))

    navigate('/')
  }

  async function login(user) {
    let msgText = 'Login realizado com sucesso!'
    let msgType = 'success'

    try {
      const data = await api.post('/users/login', user).then((response) => {
        return response.data
      })

      await authUser(data)
    } catch (err) {
      msgText = err.response.data.message
      msgType = 'error'
    }

    setFlashMessage(msgText, msgType)
  }

  async function logout() {
    let msgText = 'Logout realizado com sucesso!'
    let msgType = 'success'

    setAuthenticated(false)
    localStorage.removeItem('token')
    api.defaults.headers.Authorization = null
    navigate('/login')

    setFlashMessage(msgText, msgType)
  }

  return { authenticated, loading, register, login, logout }
}

export default useAuth
